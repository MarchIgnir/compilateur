#include <string>
#include <iostream>
#include <cstdlib>
#include <set>
#include <map>
#include <FlexLexer.h>
#include "tokeniser.h"
#include <cstring>

using namespace std;

enum OPREL {EQU, DIFF, INF, SUP, INFE, SUPE, WTFR};
enum OPADD {ADD, SUB, OR, WTFA};
enum OPMUL {MUL, DIV, MOD, AND ,WTFM};
enum TYPE {UNSIGNED_INT, BOOL}

TOKEN current;				// Current token


FlexLexer* lexer = new yyFlexLexer; // This is the flex tokeniser
// tokens can be read using lexer->yylex()
// lexer->yylex() returns the type of the lexicon entry (see enum TOKEN in tokeniser.h)
// and lexer->YYText() returns the lexicon entry as a string

	
map<string, TYPE> DeclaredVariables;
unsigned long TagNumber=0;

String TYPEtoString(TYPES t){
	switch(t){
		case UNSIGNED_INT:
			return "INT";
			break;
		case BOOL
			return "BOOL";
			break;
	}
}

bool IsDeclared(const char *id){
	return DeclaredVariables.find(id)!=DeclaredVariables.end();
}


void Error(string s){
	cerr << "Ligne n°"<<lexer->lineno()<<", lu : '"<<lexer->YYText()<<"'("<<current<<"), mais ";
	cerr<< s << endl;
	exit(-1);
}

// Program := [DeclarationPart] StatementPart
// DeclarationPart := "[" Letter {"," Letter} "]"
// StatementPart := Statement {";" Statement} "."
// Statement := AssignementStatement | IfStatement | WhileStatement | ForStatement | BlockStatement
// IfStatement : "IF" Expression "THEN" Statement [ "ELSE" Statement ]
// WhileStatement := "WHILE" Expression DO Statement
// ForStatement := "FOR" AssignementStatement "To" Expression "DO" Statement
// BlockStatement := "BEGIN" Statement { ";" Statement } "END"
// AssignementStatement := Letter "=" Expression

// Expression := SimpleExpression [RelationalOperator SimpleExpression]
// SimpleExpression := Term {AdditiveOperator Term}
// Term := Factor {MultiplicativeOperator Factor}
// Factor := Number | Letter | "(" Expression ")"| "!" Factor
// Number := Digit{Digit}

// AdditiveOperator := "+" | "-" | "||"
// MultiplicativeOperator := "*" | "/" | "%" | "&&"
// RelationalOperator := "==" | "!=" | "<" | ">" | "<=" | ">="  
// Digit := "0"|"1"|"2"|"3"|"4"|"5"|"6"|"7"|"8"|"9"
// Letter := "a"|...|"z"
	
		
TYPE Identifier(void){
	cout << "\tpush "<<lexer->YYText()<<endl;
	current=(TOKEN) lexer->yylex();
	return UNSIGNED_INT;
}

TYPE Number(void){
	cout <<"\tpush $"<<atoi(lexer->YYText())<<endl;
	current=(TOKEN) lexer->yylex();
	return UNSIGNED_INT;
}

void Expression(void);			// Called by Term() and calls Term()

TYPE Factor(void){
	TYPE t;
	if(current==RPARENT){
		current=(TOKEN) lexer->yylex();
		t = Expression();
		return t;
		if(current!=LPARENT)
			Error("')' était attendu");		// ")" expected
		else
			current=(TOKEN) lexer->yylex();
	}
	else{ 
		if (current==NUMBER){
			t = Number();
			return t;
	     	}else{
				if(current==ID){
					t = Identifier();
					return t;
				}else{
					Error("'(' ou chiffre ou lettre attendue");
				}
			}
		}
}

// MultiplicativeOperator := "*" | "/" | "%" | "&&"
OPMUL MultiplicativeOperator(void){
	OPMUL opmul;
	if(strcmp(lexer->YYText(),"*")==0)
		opmul=MUL;
	else if(strcmp(lexer->YYText(),"/")==0)
		opmul=DIV;
	else if(strcmp(lexer->YYText(),"%")==0)
		opmul=MOD;
	else if(strcmp(lexer->YYText(),"&&")==0)
		opmul=AND;
	else opmul=WTFM;
	current=(TOKEN) lexer->yylex();
	return opmul;
}

// Term := Factor {MultiplicativeOperator Factor}
TYPE Term(void){
	TYPE t, p;
	OPMUL mulop;
	t = Factor();
	return t;
	while(current==MULOP){
		mulop=MultiplicativeOperator();		// Save operator in local variable
		p = Factor();
		if(p != t){
			Error("Il y a problème de type ! ");
		}
		return p;
		cout << "\tpop %rbx"<<endl;	// get first operand
		cout << "\tpop %rax"<<endl;	// get second operand
		switch(mulop){
			case AND:
				cout << "\tmulq	%rbx"<<endl;	// a * b -> %rdx:%rax
				cout << "\tpush %rax\t# AND"<<endl;	// store result
				break;
			case MUL:
				cout << "\tmulq	%rbx"<<endl;	// a * b -> %rdx:%rax
				cout << "\tpush %rax\t# MUL"<<endl;	// store result
				break;
			case DIV:
				cout << "\tmovq $0, %rdx"<<endl; 	// Higher part of numerator  
				cout << "\tdiv %rbx"<<endl;			// quotient goes to %rax
				cout << "\tpush %rax\t# DIV"<<endl;		// store result
				break;
			case MOD:
				cout << "\tmovq $0, %rdx"<<endl; 	// Higher part of numerator  
				cout << "\tdiv %rbx"<<endl;			// remainder goes to %rdx
				cout << "\tpush %rdx\t# MOD"<<endl;		// store result
				break;
			default:
				Error("opérateur multiplicatif attendu");
		}
	}
}

// AdditiveOperator := "+" | "-" | "||"
OPADD AdditiveOperator(void){
	OPADD opadd;
	if(strcmp(lexer->YYText(),"+")==0)
		opadd=ADD;
	else if(strcmp(lexer->YYText(),"-")==0)
		opadd=SUB;
	else if(strcmp(lexer->YYText(),"||")==0)
		opadd=OR;
	else opadd=WTFA;
	current=(TOKEN) lexer->yylex();
	return opadd;
}

// SimpleExpression := Term {AdditiveOperator Term}
TYPE SimpleExpression(void){
	TYPE t, p;
	OPADD adop;
	t = Term();
	return t;
	while(current==ADDOP){
		adop=AdditiveOperator();		// Save operator in local variable
		p = Term();
		if(t != p){
			Error("Il y a un problème de type !");
		}
		cout << "\tpop %rbx"<<endl;	// get first operand
		cout << "\tpop %rax"<<endl;	// get second operand
		switch(adop){
			case OR:
				cout << "\taddq	%rbx, %rax\t# OR"<<endl;// operand1 OR operand2
				break;			
			case ADD:
				cout << "\taddq	%rbx, %rax\t# ADD"<<endl;	// add both operands
				break;			
			case SUB:	
				cout << "\tsubq	%rbx, %rax\t# SUB"<<endl;	// substract both operands
				break;
			default:
				Error("opérateur additif inconnu");
		}
		cout << "\tpush %rax"<<endl;			// store result
	}

}

// DeclarationPart := "[" Ident {"," Ident} "]"
void DeclarationPart(void){
	if(current!=RBRACKET)
		Error("caractère '[' attendu");
	cout << "\t.data"<<endl;
	cout << "\t.align 8"<<endl;
	
	current=(TOKEN) lexer->yylex();
	if(current!=ID)
		Error("Un identificater était attendu");
	cout << lexer->YYText() << ":\t.quad 0"<<endl;
	DeclaredVariables.insert(pair<string, TYPE>(lexer->YYText(), UNSIGNED_INT));
	current=(TOKEN) lexer->yylex();
	while(current==COMMA){
		current=(TOKEN) lexer->yylex();
		if(current!=ID)
			Error("Un identificateur était attendu");
		cout << lexer->YYText() << ":\t.quad 0"<<endl;
		DeclaredVariables.insert(pair<string, TYPE>(lexer->YYText(), UNSIGNED_INT));
		current=(TOKEN) lexer->yylex();
	}
	if(current!=LBRACKET)
		Error("caractère ']' attendu");
	current=(TOKEN) lexer->yylex();
}

// RelationalOperator := "==" | "!=" | "<" | ">" | "<=" | ">="  
OPREL RelationalOperator(void){
	OPREL oprel;
	if(strcmp(lexer->YYText(),"==")==0)
		oprel=EQU;
	else if(strcmp(lexer->YYText(),"!=")==0)
		oprel=DIFF;
	else if(strcmp(lexer->YYText(),"<")==0)
		oprel=INF;
	else if(strcmp(lexer->YYText(),">")==0)
		oprel=SUP;
	else if(strcmp(lexer->YYText(),"<=")==0)
		oprel=INFE;
	else if(strcmp(lexer->YYText(),">=")==0)
		oprel=SUPE;
	else oprel=WTFR;
	current=(TOKEN) lexer->yylex();
	return oprel;
}

// Expression := SimpleExpression [RelationalOperator SimpleExpression]
TYPE Expression(void){
	TYPE t, p;
	OPREL oprel;
	t = SimpleExpression();
	if(current==RELOP){
		oprel=RelationalOperator();
		p = SimpleExpression();
		if(t != p){
			Error("Il y a un problème de type !");
		}
		cout << "\tpop %rax"<<endl;
		cout << "\tpop %rbx"<<endl;
		cout << "\tcmpq %rax, %rbx"<<endl;
		switch(oprel){
			case EQU:
				cout << "\tje Vrai"<<++TagNumber<<"\t# If equal"<<endl;
				break;
			case DIFF:
				cout << "\tjne Vrai"<<++TagNumber<<"\t# If different"<<endl;
				break;
			case SUPE:
				cout << "\tjae Vrai"<<++TagNumber<<"\t# If above or equal"<<endl;
				break;
			case INFE:
				cout << "\tjbe Vrai"<<++TagNumber<<"\t# If below or equal"<<endl;
				break;
			case INF:
				cout << "\tjb Vrai"<<++TagNumber<<"\t# If below"<<endl;
				break;
			case SUP:
				cout << "\tja Vrai"<<++TagNumber<<"\t# If above"<<endl;
				break;
			default:
				Error("Opérateur de comparaison inconnu");
		}
		cout << "\tpush $0\t\t# False"<<endl;
		cout << "\tjmp Suite"<<TagNumber<<endl;
		cout << "Vrai"<<TagNumber<<":\tpush $0xFFFFFFFFFFFFFFFF\t\t# True"<<endl;	
		cout << "Suite"<<TagNumber<<":"<<endl;
		t = BOOL;
	}
		return t;
}

// AssignementStatement := Identifier ":=" Expression
TYPE AssignementStatement(void){
	TYPE t;
	string variable;
	if(current!=ID)
		Error("Identificateur attendu");
	if(!IsDeclared(lexer->YYText())){
		cerr << "Erreur : Variable '"<<lexer->YYText()<<"' non déclarée"<<endl;
		exit(-1);
	}
	variable=lexer->YYText();
	current=(TOKEN) lexer->yylex();
	if(current!=ASSIGN)
		Error("caractères ':=' attendus");
	current=(TOKEN) lexer->yylex();
	t = Expression();
	if(t != DeclaredVariables){
		Error("Il y a un problème de type entre la variable et l'expression !");
	}
	cout << "\tpop "<<variable<<endl;
}

// Statement := AssignementStatement
void Statement(void){
if (current==ID) {
        AssignementStatement();
    }
    else if (current==KEYWORD && strcmp(lexer->YYText(),"IF")==0) {
        IfStatement();
    }
    else if (current==KEYWORD && strcmp(lexer->YYText(),"WHILE")==0) {
        WhileStatement();
    }
    else if (current==KEYWORD && strcmp(lexer->YYText(),"FOR")==0) {
        ForStatement();
    }
    else if (current==KEYWORD && strcmp(lexer->YYText(),"BEGIN")==0) {
        BlockStatement();
    }

}

// StatementPart := Statement {";" Statement} "."
void StatementPart(void){
	cout << "\t.text\t\t# The following lines contain the program"<<endl;
	cout << "\t.globl main\t# The main function must be visible from outside"<<endl;
	cout << "main:\t\t\t# The main function body :"<<endl;
	cout << "\tmovq %rsp, %rbp\t# Save the position of the stack's top"<<endl;
	Statement();
	while(current==SEMICOLON){
		current=(TOKEN) lexer->yylex();
		Statement();
	}
	if(current!=DOT)
		Error("caractère '.' attendu");
	current=(TOKEN) lexer->yylex();
}

void IfStatement(void){
	int tag = ++ TagNumber;
	current=(TOKEN) lexer->yylex();
	if(current == KEYWORD && strcmp(lexer->YYText(),"IF")==0){
		cout << "IF" <<  tag << endl;
		current = (TOKEN) lexer->yylex();
		Expression();
		cout << "\t pop %rax" << endl;
		cout << "\t cmpq $0 %rax" << endl;
		cout << "\t je ELSE" << endl;
		cout << "THEN" << tag << endl;
		if(current == KEYWORD && strcmp(lexer->YYText(),"THEN")==0){
			current = (TOKEN) lexer->yylex();
			Statement();
			cout << "\t jmp FinSi" << endl;
			cout << "ELSE" << tag << endl;
			if(current == KEYWORD && strcmp(lexer->YYText(),"ELSE")==0){
				current = (TOKEN) lexer->yylex();
				Statement();
			}else{
				Error("THEN attendue");
			}
			cout << "\t FinSi :" << endl;
		}
	}else{
		Error("IF attendue");
	}
}

void WhileStatement(void){
	int tag = ++ TagNumber;
	current=(TOKEN) lexer->yylex();
	if(current == KEYWORD && strcmp(lexer->YYText(), "WHILE")==0){
		cout << "WHILE" << endl;
		current = (TOKEN) lexer ->yylex();
		Expression();
		cout << "DO" << tag << endl;
		if(current == KEYWORD && strcmp(lexer->YYText(), "DO")==0){
			current = (TOKEN) lexer->yylex();
			Statement();
		}
	}
}

void ForStatement(void){
	int tag = ++ TagNumber;
	current = (TOKEN) lexer->yylex();
	if(current == KEYWORD && strcmp(lexer->YYText(), "FOR")==0){
		cout << "FOR" << endl;
		current = (TOKEN) lexer->yylex();
		AssignementStatement();
		cout << tag << endl;
		if(current == KEYWORD && strcmp(lexer->YYText(), "TO")==0){
			cout << "TO" << endl;
			current = (TOKEN) lexer->yylex();
			Expression();
			cout << tag << endl;
			if(current == KEYWORD && strcmp(lexer->YYText(), "DO")==0){
				cout << "DO" << endl;
				current = (TOKEN) lexer->yylex();
				Statement();
				cout << tag << endl;
			}else{
				Error("DO attendue");
			}
		}else{
			Error("TO attendue");
		}
	}else{
		Error("FOR attendue");
	}
}

void BlockStatement(void){
	int tag = ++ TagNumber;
	current = (TOKEN) lexer->yylex();
	if(current == KEYWORD && strcmp(lexer->YYText(), "BEGIN")==0){
		cout << "BEGIN" << endl;
		current = (TOKEN) lexer->yylex();
		Statement();
		cout << tag << endl;
		while(current == SEMICOLON){
			current = (TOKEN) lexer->yylex();
			Statement();
		}
		if(current == KEYWORD && strcmp(lexer->YYText(), "END")==0){
			cout << "END" << endl;
			current = (TOKEN) lexer->yylex();
			cout << tag << endl;
		}else {
			Error("END attendue");
		}
	}else{
		Error("BEGIN attendue");
	}
}

// Program := [DeclarationPart] StatementPart
void Program(void){
	if(current==RBRACKET)
		DeclarationPart();
	StatementPart();	
}

int main(void){	// First version : Source code on standard input and assembly code on standard output
	// Header for gcc assembler / linker
	cout << "\t\t\t# This code was produced by the CERI Compiler"<<endl;
	// Let's proceed to the analysis and code production
	current=(TOKEN) lexer->yylex();
	Program();
	// Trailer for the gcc assembler / linker
	cout << "\tmovq %rbp, %rsp\t\t# Restore the position of the stack's top"<<endl;
	cout << "\tret\t\t\t# Return from main function"<<endl;
	if(current!=FEOF){
		cerr <<"Caractères en trop à la fin du programme : ["<<current<<"]";
		Error("."); // unexpected characters at the end of program
	}

}
		
			





